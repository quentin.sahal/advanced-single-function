import SQS from "aws-sdk/clients/sqs";

const sqsClient = new SQS({ apiVersion: "2012-11-05", region: process.env.AWS_REGION });

export default sqsClient;
