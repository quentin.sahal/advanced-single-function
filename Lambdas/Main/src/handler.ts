import type { APIGatewayProxyEvent, APIGatewayProxyResult, Context } from "aws-lambda";

export const handler = async (
  event: APIGatewayProxyEvent,
  context: Context,
): Promise<APIGatewayProxyResult> => {
  console.log({ event, context });
  return {
    statusCode: 200,
    body: JSON.stringify({ hello: "op" }),
  };
};
