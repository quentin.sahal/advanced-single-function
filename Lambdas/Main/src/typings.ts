export interface ITableClient<T> {
  getItem: (key: string) => Promise<(T & ItemMetadata) | undefined>;
  putItem: (data: T, opts?: PutItemOpts) => Promise<PutItemOutput>; // @TODO: Fix type
  increment(hashKeyValue: string | number, countKey: keyof T, increase?: number): Promise<void>;
}

export type PutItemOpts = {
  createOnly: boolean;
};

export type PutItemOutput = {
  error?: Error;
  success?: boolean;
};

export type ItemMetadata = {
  expires: number;
  createdAt: number;
};

export type DonGigas = {
  personId: string;
  partnerId: string;
};
