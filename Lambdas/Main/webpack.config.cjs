const path = require("path");
const { DefinePlugin } = require("webpack");
const { CleanWebpackPlugin } = require("clean-webpack-plugin");
const nodeExternals = require("webpack-node-externals");

const pkg = require("./package.json");

module.exports = (env) => ({
  mode: "production",
  entry: path.resolve(__dirname, "./src/handler.ts"),
  output: {
    path: path.resolve(__dirname, `build`),
    libraryTarget: "commonjs",
    filename: "handler.js",
  },
  devtool: env.mode === "development" ? "inline-source-map" : false,
  target: "node",
  externalsPresets: { node: true },
  externals: [
    nodeExternals({
      // Don't package aws-sdk
      // because already in the runtime
      allowlist: [/^(?!.*(aws-sdk))/],
    }),
  ],
  devtool: false,
  module: {
    rules: [
      {
        test: /.ts?$/,
        use: "ts-loader",
        exclude: /(node_modules)\//,
      },
    ],
  },
  plugins: [
    new CleanWebpackPlugin(),
    new DefinePlugin({
      "process.env.VERSION": JSON.stringify(pkg.version),
    }),
    // new CopyWebpackPlugin({
    //   patterns: [{ from: "./src/env.config.json", to: "." }],
    // }),
  ],
  resolve: {
    modules: [path.resolve(process.cwd(), "node_modules")],
    extensions: [".ts", ".js"],
  },
});
