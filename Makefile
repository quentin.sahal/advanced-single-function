log = (echo "--> $1")

# CONSTANTS
PRODUCTION 					:= production
DEVELOPMENT					:= development

# ENVIRONMENT VARIABLES
ENV 						:= $(PRODUCTION)
LOCAL 						:= 1 # 0: False |  1: True
AWS_REGION					:= eu-west-3
BUCKET						:= aws-sam-cli-managed-default-samclisourcebucket-8udtje0rytwr
LOCAL_NETWORK				:= local-sls



## COMMANDS
build-app:
	@$(call log, "Build App")
	cd Lambdas/Main && yarn build
	sam build -t Infrastructure/template.yaml

check-local-env-status:
	@docker network ls | grep $(LOCAL_NETWORK) > /dev/null
	@$(call log, "Env is Ready")

deploy-app: build-app
	@$(call log, "Deploy App")
	sam deploy --stack-name sam-stack-advanced-single-function \
		--region $(AWS_REGION)  \
		--resolve-s3 \
		--no-confirm-changeset \
		--capabilities CAPABILITY_IAM CAPABILITY_NAMED_IAM \
		--parameter-overrides Env=$(ENV) \
		--template-file Infrastructure/template.yaml

destroy-local:
	@$(call log, "Destroy Local Env")
	@docker network rm $(LOCAL_NETWORK)

run-local-rebuild: build-app run-local

run-local:
	@$(call log, "Run App Locally")
	sam local start-api \
		--parameter-overrides Env=$(DEVELOPMENT) \
		--docker-network $(LOCAL_NETWORK)

setup-local: create-local-network
	@$(call log, "Local Env is Set Up")


## HELPERS
create-local-network:
	@docker network create $(LOCAL_NETWORK) || true
